//
// Created by Nathan Evans on 5/16/19.
//

#ifndef IN_CLASS_TCP_CLIENT_UTIL_H
#define IN_CLASS_TCP_CLIENT_UTIL_H

#import <sys/socket.h>
#import <netdb.h>
#import <iostream>
#import <errno.h>

const char *printable_address(struct sockaddr_storage *client_addr, socklen_t client_addr_len);

#endif //IN_CLASS_TCP_CLIENT_UTIL_H


