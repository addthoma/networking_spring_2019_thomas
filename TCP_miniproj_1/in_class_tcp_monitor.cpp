#include <iostream>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netdb.h>
#include <poll.h>
#include "tcp_chat.h"

struct Packet{
  uint16_t name_len;
  uint16_t msg_len;
};
/**
 *
 * TCP client example. Reads in IP PORT
 * from the command line, and sends DATA via TCP to IP:PORT.
 *
 * e.g., ./tcpclient 127.0.0.1 8888
 *
 * @param argc count of arguments on the command line
 * @param argv array of command line arguments
 * @return 0 on success, non-zero if an error occurred
 */
int main(int argc, char *argv[]) {
  // Alias for argv[1] for convenience
  char *ip_string;
  // Alias for argv[2] for convenience
  char *port_string;
  // Alias for argv[3] for convenience
  char *data_string;

  // Port to send TCP data to. Need to convert from command line string to a number
  unsigned int port;
  // The socket used to send data
  int tcp_socket;
  // Variable used to check return codes from various functions
  int ret;


  struct addrinfo hints;
  struct addrinfo *results;
  struct addrinfo *results_it;

  // Note: this needs to be 4, because the program name counts as an argument!
  if (argc < 3) {
    std::cerr << "Please specify IP PORT as first two arguments." << std::endl;
    return 1;
  }
  // Set up variables "aliases"
  ip_string = argv[1];
  port_string = argv[2];

  // Create the TCP socket.
  // AF_INET is the address family used for IPv4 addresses
  // SOCK_STREAM indicates creation of a TCP socket
  tcp_socket = socket(AF_INET, SOCK_STREAM, 0);

  // Make sure socket was created successfully, or exit.
  if (tcp_socket == -1) {
    std::cerr << "Failed to create tcp socket!" << std::endl;
    std::cerr << strerror(errno) << std::endl;
    return 1;
  }
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_addr = NULL;
  hints.ai_canonname = NULL;
  hints.ai_family = AF_INET;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_socktype = SOCK_STREAM;
  // Instead of using inet_pton, use getaddrinfo to convert.
  ret = getaddrinfo(ip_string, port_string, &hints, &results);

  if (ret != 0) {
    std::cerr << "Getaddrinfo failed with error " << ret << std::endl;
    perror("getaddrinfo");
    return 1;
  }

  // Check we have at least one result
  results_it = results;

  while (results_it != NULL) {
    std::cout << "Trying to connect to something?" << std::endl;
    ret = connect(tcp_socket, results_it->ai_addr, results_it->ai_addrlen);
    if (ret == 0) {
      break;
    }
    perror("connect");
    results_it = results_it->ai_next;
  }

  // Whatever happened, we need to free the address list.
  freeaddrinfo(results);

  // Check if connecting succeeded at all
  if (ret != 0) {
    std::cout << "Failed to connect to any addresses!" << std::endl;
    return 1;
  }

  ChatMonMsg *monMes = new ChatMonMsg;
  monMes->type = htons(675);
  monMes->data_len = htons(0);
  monMes->nickname_len = htons(0);

  ret = send(tcp_socket, monMes, sizeof(ChatMonMsg),0);
  std::cout<<"Sent " << ret << " bytes of data" << '\n';

  pollfd pfds[2];
  pfds[0].fd = tcp_socket;
  pfds[0].events = POLLIN;
  pfds[1].fd = 0;
  pfds[1].events = POLLIN;
  int num = 0;

  while(true) {
    //poll the server for new data
    num = poll(pfds,2,5000);
    //buffer for the data
    char buff[2048];

    //check to see if data present on tcpsocket
    if (pfds[0].revents & POLLIN) {

      //read it into buffer.
      ret = recv(pfds[0].fd, buff,2048,0);
      //add null terminator
      buff[ret] = '\0';


      ChatMonMsg* recMes = (ChatMonMsg*) buff;

      char* nickname = (char*) malloc(ntohs(recMes->nickname_len));

      if(ntohs(recMes->type) == MON_MESSAGE) {

        int offset = 0;

      while(offset<ret) {

      if(ntohs(recMes->data_len) + ntohs(recMes->nickname_len) + sizeof(ChatMonMsg) <= ret) {
          char* data = (char*) malloc(ntohs(recMes->data_len));
          recMes = (ChatMonMsg*) &buff[offset];
          memcpy(nickname, &recMes[1],ntohs(recMes->nickname_len));
          memcpy(data,buff+sizeof(ChatMonMsg)+ntohs(recMes->nickname_len) ,ntohs(recMes->data_len));
          offset+=sizeof(ChatMonMsg)+ntohs(recMes->nickname_len)+ntohs(recMes->data_len);
          std::cout<<data<<'\n';

      }

      }
      } else {
        //std::cout << "MON_DIRECT_MESSAGE" << '\n';

      }
    } else if (pfds[1].revents & POLLIN) {
      ret = read(pfds[1].fd,buff,2048);
    //  std::cout << buff << '\n';
      buff[ret] = '\0';
      if(strncmp(buff, "quit",4) == 0) {
        ChatMonMsg *exiting = new ChatMonMsg;
        exiting->type = htons(676);
        exiting->data_len = htons(0);
        exiting->nickname_len = htons(0);
        ret = send(tcp_socket, &exiting, sizeof(ChatMonMsg),0);
        exit(0);
      }
    }

  // Check if send worked, clean up and exit if not.
  if (ret == -1) {
    std::cerr << "Failed to send data!" << std::endl;
    perror("send");
    std::cerr << strerror(errno) << std::endl;
    close(tcp_socket);
    return 1;
  }

}
  close(tcp_socket);
  return 0;
}
