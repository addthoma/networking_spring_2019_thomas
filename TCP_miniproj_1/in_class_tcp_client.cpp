#include <iostream>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <netdb.h>
#include <poll.h>
#include "tcp_chat.h"

struct Packet{
  uint16_t name_len;
  uint16_t msg_len;
};
/**
 *
 * TCP client example. Reads in IP PORT
 * from the command line, and sends DATA via TCP to IP:PORT.
 *
 * e.g., ./tcpclient 127.0.0.1 8888
 *
 * @param argc count of arguments on the command line
 * @param argv array of command line arguments
 * @return 0 on success, non-zero if an error occurred
 */
int main(int argc, char *argv[]) {
  // Alias for argv[1] for convenience
  char *ip_string;
  // Alias for argv[2] for convenience
  char *port_string;
  // Alias for argv[3] for convenience
  char *data_string;

  // Port to send TCP data to. Need to convert from command line string to a number
  unsigned int port;
  // The socket used to send data
  int tcp_socket;
  // Variable used to check return codes from various functions
  int ret;

  bool name = false;

  struct addrinfo hints;
  struct addrinfo *results;
  struct addrinfo *results_it;

  // Note: this needs to be 4, because the program name counts as an argument!
  if (argc < 3) {
    std::cerr << "Please specify IP PORT as first two arguments." << std::endl;
    return 1;
  }
  // Set up variables "aliases"
  ip_string = argv[1];
  port_string = argv[2];

  if(argc > 3) {
  data_string = argv[3];
  name = true;
 }

 if(!name) {
   std::cout << "Please input a nickname" << std::endl;
   std::string s;
   std::cin >> s;
   data_string = new char[s.size() + 1];
   strcpy(data_string, s.c_str());
 }
  // Create the TCP socket.
  // AF_INET is the address family used for IPv4 addresses
  // SOCK_STREAM indicates creation of a TCP socket
  tcp_socket = socket(AF_INET, SOCK_STREAM, 0);

  // Make sure socket was created successfully, or exit.
  if (tcp_socket == -1) {
    std::cerr << "Failed to create tcp socket!" << std::endl;
    std::cerr << strerror(errno) << std::endl;
    return 1;
  }
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_addr = NULL;
  hints.ai_canonname = NULL;
  hints.ai_family = AF_INET;
  hints.ai_protocol = 0;
  hints.ai_flags = AI_PASSIVE;
  hints.ai_socktype = SOCK_STREAM;
  // Instead of using inet_pton, use getaddrinfo to convert.
  ret = getaddrinfo(ip_string, port_string, &hints, &results);

  if (ret != 0) {
    std::cerr << "Getaddrinfo failed with error " << ret << std::endl;
    perror("getaddrinfo");
    return 1;
  }

  // Check we have at least one result
  results_it = results;

  while (results_it != NULL) {
    std::cout << "Trying to connect to something?" << std::endl;
    ret = connect(tcp_socket, results_it->ai_addr, results_it->ai_addrlen);
    if (ret == 0) {
      break;
    }
    perror("connect");
    results_it = results_it->ai_next;
  }

  // Whatever happened, we need to free the address list.
  freeaddrinfo(results);

  // Check if connecting succeeded at all
  if (ret != 0) {
    std::cout << "Failed to connect to any addresses!" << std::endl;
    return 1;
  }

  //Chat client message with CLIENT_CONNECT type
  ChatClientMessage *mes = new ChatClientMessage;
  mes->type = htons(CLIENT_CONNECT);
  mes->nickname_len = htons(0);
  mes->data_length = htons(0);

  // If we get here, the connect worked. Now send the data.
  ret = send(tcp_socket, mes, sizeof(ChatClientMessage), 0);

  std::cout << "Sent " << ret << " bytes " << std::endl;


  ChatClientMessage *nick = (ChatClientMessage *) malloc(sizeof(ChatClientMessage) + strlen(data_string));
  nick->type = htons(CLIENT_SET_NICKNAME);
  nick->nickname_len = htons(0);
  nick->data_length = htons(strlen(data_string));
  memcpy(&nick[1],data_string,strlen(data_string));

  ret = send(tcp_socket, nick, sizeof(ChatClientMessage)+strlen(data_string), 0);

  pollfd pfds[2];
  pfds[0].fd = 0;
  pfds[1].fd = tcp_socket;
  pfds[0].events = POLLIN;
  pfds[1].events = POLLIN;
  char buff[2048];
  int num = 0;

  while(true) {

    num = poll(pfds,2,5000);

    if(pfds[0].revents & POLLIN) {

      ret = read(pfds[0].fd,buff,2048);
      if(ret!=0){
      buff[ret] = '\0';
    }

      if(strncmp(buff, "quit",4) == 0) {
        std::cout << "Got Exit" << '\n';
        ChatClientMessage *exiting = new ChatClientMessage;
        exiting->type = htons(11);
        exiting->data_length = htons(0);
        exiting->nickname_len = htons(0);
        ret = send(tcp_socket,exiting, sizeof(ChatClientMessage),0);
        exit(0);
      } else if(strncmp(buff, "list",4) == 0) {
        std::cout << "Listing..." << '\n';
        ChatClientMessage *list = new ChatClientMessage;
        list->type = htons(CLIENT_GET_MEMBERS);
        list->data_length = htons(0);
        list->nickname_len = htons(0);
        ret = send(tcp_socket,list, sizeof(ChatClientMessage),0);
      }

      ChatClientMessage *sending;

      if(buff[0] == '/') {

        sending = (ChatClientMessage *) malloc(sizeof(ChatClientMessage) + strlen(buff));
        sending->type = htons(CLIENT_SEND_DIRECT_MESSAGE);

        int size=0;

        for(int i = 1; buff[i]!= '/';i++) {
          size++;
        }

        sending->nickname_len = htons(size);
        sending->data_length = htons(strlen(buff));

        memcpy(&sending[1],buff,strlen(buff));
        ret = send(tcp_socket,sending,sizeof(ChatClientMessage) + strlen(buff),0);
      } else {
      sending = (ChatClientMessage *) malloc(sizeof(ChatClientMessage) + strlen(buff));
      sending->type = htons(CLIENT_SEND_MESSAGE);
      sending->nickname_len = htons(0);
      sending->data_length = htons(strlen(buff));
      memcpy(&sending[1],buff,strlen(buff));
      ret = send(tcp_socket,sending,sizeof(ChatClientMessage) + strlen(buff),0);
    }

      std::cout << ret << " Bytes sent" << '\n';


  }

  //  send->data_len =
  //  ret = poll(fds, 2, int timeout);

  // Check if send worked, clean up and exit if not.
  if (ret == -1) {
    std::cerr << "Failed to send data!" << std::endl;
    perror("send");
    std::cerr << strerror(errno) << std::endl;
    close(tcp_socket);
    return 1;
  }

  //std::cout << "Sent " << ret << " bytes " << std::endl;
}
  close(tcp_socket);
  return 0;
}
