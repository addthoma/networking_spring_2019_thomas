//
// Created by Nathan Evans on 5/16/19.
//

#include "util.h"

/***
 *
 * @param client_addr ipv4 or v6 address pointer, cast to sockaddr_storage (this is safe as long as it's got a valid length)
 * @param client_addr_len length of client_addr, again, safe as long as length is correct
 * @return pointer to *static* char buffer with the address in it
 *
 * Non-re-entrant!
 */
const char *printable_address(struct sockaddr_storage *client_addr, socklen_t client_addr_len) {
  // Buffer will be big enough for either a v4 or v6 address
  // AND big enough to put :65535 (the port) at the end.
  static char print_buf[NI_MAXHOST + NI_MAXSERV];
  static char host_buf[NI_MAXHOST];
  static char port_buf[NI_MAXSERV];

  int ret;
  // Verify address family is either v4 or v6
  switch (client_addr->ss_family) {
    case AF_INET:
      break;
    case AF_INET6:
      break;
    default:
      return nullptr;
  }

  // If we get here, we're good to go!
  ret = getnameinfo((struct sockaddr *)client_addr, client_addr_len,
                    host_buf, NI_MAXHOST,
                    port_buf, NI_MAXSERV, NI_NUMERICHOST | NI_NUMERICSERV);
  if (ret != 0) {
    std::cout << "getnameinfo error " << gai_strerror(errno) << std::endl;
    return nullptr;
  }

  strncpy(print_buf, host_buf, NI_MAXHOST);
  print_buf[strlen(host_buf)] = ':';
  strncpy(&print_buf[strlen(host_buf) + 1], port_buf, NI_MAXSERV);

  return print_buf;
}