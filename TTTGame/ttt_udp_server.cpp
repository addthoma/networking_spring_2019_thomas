#include <iostream>
#include <sys/socket.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "tictactoe.h"
#include <time.h>
#include <random>
#include <vector>
#include <algorithm>
#include <map>
#include <iostream>
#include <iterator>
#include <cstring>

//Questions:
/*
1) why does the program stop sending data ofter a dozen or so times.
2) How do I make the distribution better?
3) Why wont this work on the linux server?

*/

#define ERROR_MSG "WE ENCOUNTERED AN ERROR error code"

void handle_error(const char *context) {
  std::cerr << context << " failed with error:" << std::endl;
  std::cerr << strerror(errno) << std::endl;
  return;
}

int send_udp_buffer(int udp_socket, char *send_buf, uint16_t send_buf_size,
                    struct sockaddr *send_addr, socklen_t send_addr_size) {
  int ret;
  ret = sendto(udp_socket, send_buf, send_buf_size, 0, send_addr, send_addr_size);
  if (ret == -1) {
    handle_error("sendto");
  }
  return ret;
}


int main(int argc, char *argv[]) {
  std::map<uint16_t, uint16_t> * map = new std::map<uint16_t, uint16_t>();

while(true) {

  char *ip_str;
  char *port_str;

  int udp_socket;
  unsigned int port;
  struct sockaddr_in dest;

  static char recv_buf[2048];
  struct sockaddr_in recv_addr;
  socklen_t recv_addr_size;

  static char send_buf[2048];
  int random_number;
  int ret;
  int bytes_printed;

  //map that contains the game_ids as a key and the win scenario as a value

  if (argc < 3) {
    std::cerr << "Provide IP PORT as first two arguments." << std::endl;
    return 1;
  }
  ip_str = argv[1];
  port_str = argv[2];

  udp_socket = socket(AF_INET, SOCK_DGRAM, 0);

  if (udp_socket == -1) {
    std::cerr << "Failed to create socket with error:" << std::endl;
    std::cerr << strerror(errno) << std::endl;
    return 1;
  }

  ret = inet_pton(AF_INET, ip_str, &dest.sin_addr);

  if (ret == -1) {
    handle_error("inet_pton");
    return 1;
  }

  ret = sscanf(port_str, "%u", &port);

  if (ret != 1) {
    handle_error("sscanf");
  }

  dest.sin_family = AF_INET;
  //dest.sin_len = sizeof(struct sockaddr_in);
  dest.sin_port = htons(port);

  bind(udp_socket, (struct sockaddr *) &dest, sizeof(struct sockaddr_in));
  //ret = sendto(udp_socket, data_str, strlen(data_str), 0, (struct sockaddr *)&dest, sizeof(struct sockaddr_in));

  recv_addr_size = sizeof(struct sockaddr_in);



  ret = recvfrom(udp_socket, recv_buf, 2047, 0, (struct sockaddr *) &recv_addr, &recv_addr_size);

  if (ret == -1) {
    handle_error("recvfrom");
    close(udp_socket);
    return 1;
  }

  std::cout << "Received " << ret << " bytes from address size " << recv_addr_size << std::endl;


  if(ret == sizeof(struct GetGameMessage)) {
    //create a get game message from the rec'ed data and use it to create a game summary.
    struct GetGameMessage *game;
    game = (struct GetGameMessage *)recv_buf;
    game->hdr.type = ntohs(game->hdr.type);
    game->hdr.len = ntohs(game->hdr.len);
    game->client_id = ntohs(game->client_id);

    std::cout << "Get game request from client id " << game->client_id << '\n';
    struct GameSummaryMessage reply;

    reply.hdr.type = htons(112);
    reply.hdr.len = htons(sizeof(GameSummaryMessage));
    reply.client_id = htons(game->client_id);
    reply.game_id = htons(rand());

    //use this vector to create a series of x and o games
    std::vector<uint16_t> v;
    for(int i=0;i<9;i++){
      v.push_back(i);
    }

    //declare the x and o positions.
    reply.x_positions = 0;
    reply.o_positions = 0;

    //shuffle and create the x's
    std::random_shuffle ( v.begin(), v.end() );
    for(int i =0;i<4;i++) {
      uint16_t k = v[i];
      uint16_t ONE_CONST = 1;
      ONE_CONST = ONE_CONST << k;
      reply.x_positions = reply.x_positions | ONE_CONST;
    }
    v.erase(v.begin(), v.begin()+4);
    //shuffle and create the o's
    std::random_shuffle ( v.begin(), v.end() );
    for(int i =0;i<4;i++) {
      uint16_t k = v[i];
      uint16_t ONE_CONST = 1;
      ONE_CONST = ONE_CONST << k;
      reply.o_positions = reply.o_positions | ONE_CONST;
    }
    std::cout << "Sending game. X: " << reply.x_positions << " O: " << reply.o_positions << '\n';

    //this will be the result that we use for the map.
    uint16_t result = 0;

    //create the game board
    char board[9];
    //use the flag to determine if the game is invalid.
    bool flag = false;
    //check to see if there are any x's in o's positions, and create the board array
    for(int i = 0; i<9;i++) {
      board[i] = ' ';
    }

    for(int i = 0; i<9;i++) {
      int xtemp = (reply.x_positions >> i) & 1;
      int ytemp = (reply.o_positions >> i) & 1;
      if(xtemp==1 && ytemp == 1) {
      result = INVALID_BOARD;
      board[i] = 'B';
      flag = true;
      }
      else if(xtemp == 1) {
        board[i] = 'X';
      } else if(ytemp == 1) {
        board[i] = 'O';
      }   else {
        board[i] = ' ';
      }

    }

    //output the board
  //  if(!quiet) {
      std::cout << " \n                   Tic Tac Toe" << '\n';
      std::cout << "            Player 1 (X) - Player 2 (O)" << '\n';
      std::cout << "     |     |     " << std::endl
                << "  "<<board[0]<<"  |  "<<board[1]<<"  |  "<<board[2]<<"  " << std::endl
                << "_____|_____|_____" << std::endl
                << "     |     |     " << std::endl
                << "  "<<board[3]<<"  |  "<<board[4]<<"  |  "<<board[5]<<"  " << std::endl
                << "_____|_____|_____" << std::endl
                << "     |     |     " << std::endl
                << "  "<<board[6]<<"  |  "<<board[7]<<"  |  "<<board[8]<<"  " << std::endl
                << "     |     |     " << std::endl;
    //}

      bool xtemp = false;
      bool ytemp = false;


    //determine the win conditions to see who won.
    if((board[0]=='X' && board[1]=='X' && board[2]=='X') ||
       (board[3]=='X' && board[4]=='X' && board[5]=='X') ||
       (board[6]=='X' && board[7]=='X' && board[8]=='X') ||
       (board[0]=='X' && board[3]=='X' && board[6]=='X') ||
       (board[1]=='X' && board[4]=='X' && board[7]=='X') ||
       (board[2]=='X' && board[5]=='X' && board[8]=='X') ||
       (board[0]=='X' && board[4]=='X' && board[8]=='X') ||
       (board[2]=='X' && board[4]=='X' && board[6]=='X')) {
         result = (X_WIN);
         xtemp = true;
       }
    if ((board[0]=='O' && board[1]=='O' && board[2]=='O') ||
          (board[3]=='O' && board[4]=='O' && board[5]=='O') ||
          (board[6]=='O' && board[7]=='O' && board[8]=='O') ||
          (board[0]=='O' && board[3]=='O' && board[6]=='O') ||
          (board[1]=='O' && board[4]=='O' && board[7]=='O') ||
          (board[2]=='O' && board[5]=='O' && board[8]=='O') ||
          (board[0]=='O' && board[4]=='O' && board[8]=='O') ||
          (board[2]=='O' && board[4]=='O' && board[6]=='O')) {
            result = (O_WIN);
            ytemp = true;
          }
    if(xtemp == false && ytemp == false) {
            result = (CATS_GAME);
        }
    if (xtemp && ytemp) {
          flag = true;
        }

      if(flag) {
        result = (INVALID_BOARD);
      }

    std::pair<uint16_t, uint16_t> * pair = new std::pair<uint16_t,uint16_t>(ntohs(reply.game_id),result);
    map->insert(*pair);
    std::cout << "Expected result back from the client is: " << result << '\n';

    reply.x_positions = htons(reply.x_positions);
    reply.o_positions = htons(reply.o_positions);

    ret = sendto(udp_socket,&reply,sizeof(GameSummaryMessage),0,(struct sockaddr*)&recv_addr,recv_addr_size);

  }

   else if(ret == sizeof(GameResultMessage)) {

    struct GameResultMessage * res;
    res = (struct GameResultMessage *)recv_buf;
    res->hdr.type = ntohs(res->hdr.type);
    res->hdr.len = ntohs(res->hdr.len);
    res->game_id = ntohs(res->game_id);
    res->result = ntohs(res->result);

    std::cout << "Recieved Game Result Message: " << res->result << " for game id: " << res->game_id <<'\n';

    bool found = false;
    int compareVal;
    std::map<uint16_t,uint16_t>::iterator it;

    for( it = map->begin(); it != map->end(); it++ ) {
      if (it->first == res->game_id) {
        compareVal = map->at(res->game_id);
        found = true;
      }
    }

    if(!found) {
      compareVal = -1;
    }

    if(compareVal == -1) {
      std::cout << "Received NonGameResult structure" << std::endl;
      struct TTTMessage incorrect;
      incorrect.type = htons(113);
      incorrect.len = htons(sizeof(TTTMessage));
      ret = sendto(udp_socket,&incorrect,sizeof(TTTMessage),0,(struct sockaddr*) &recv_addr, recv_addr_size);

    } else {

    struct TTTMessage mes;
    mes.len = htons(sizeof(TTTMessage));

    if(compareVal == res->result) {
      mes.type = htons(115);
      std::cout << "Sending CORRECT response to the client!" << "\n\n";
    } else {
      mes.type = htons(116);
      std::cout << "Sending INCORRECT response to the client!" << "\n\n";
    }

    ret = sendto(udp_socket,&mes,sizeof(TTTMessage),0,(struct sockaddr*) &recv_addr, recv_addr_size);
  }
  }

  else {
    std::cout << "Received NonGameResult structure" << std::endl;
    struct TTTMessage incorrect;
    incorrect.type = htons(113);
    incorrect.len = htons(sizeof(TTTMessage));
    ret = sendto(udp_socket,&incorrect,sizeof(TTTMessage),0,(struct sockaddr*) &recv_addr, recv_addr_size);

  }
  close(udp_socket);
}

  return 0;
}
