/**
 * In-class demonstrated UDP client example.
 */

#include <iostream>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include "tictactoe.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

struct DummyHeader {
    uint16_t type;
    uint16_t len;
};

struct Dummy {
    struct DummyHeader hdr;
    uint16_t magic_number;
    uint16_t age;
};

/**
 *
 * Dead simple UDP client example. Reads in IP PORT DATA
 * from the command line, and sends DATA via UDP to IP:PORT.
 *
 * e.g., ./udpclient 127.0.0.1 8888 this_is_some_data_to_send
 *
 * @param argc count of arguments on the command line
 * @param argv array of command line arguments
 * @return 0 on success, non-zero if an error occurred
 */
int main(int argc, char *argv[]) {
  // Alias for argv[1] for convenience
  char *ip_string;
  // Alias for argv[2] for convenience
  char *port_string;
  // Alias for argv[3] for convenience
  char *data_string;

  static char *ZERO_STRING = (char *)"0";


  // Port to send UDP data to. Need to convert from command line string to a number
  unsigned int port;
  // The socket used to send UDP data on
  int udp_socket;
  // Variable used to check return codes from various functions
  int ret;
  bool quiet;
  // IPv4 structure representing and IP address and port of the destination
  struct sockaddr_in dest_addr;

  // Set dest_addr to all zeroes, just to make sure it's not filled with junk
  // Note we could also make it a static variable, which will be zeroed before execution
  memset(&dest_addr, 0, sizeof(struct sockaddr_in));

  // Note: this needs to be 3, because the program name counts as an argument!
  if (argc < 3) {
    std::cerr << "Please specify IP PORT as first two arguments." << std::endl;
    return 1;
  }
  if(argc < 4) {
    quiet = false;
  } else {
    if(std::strcmp(argv[3],ZERO_STRING)==0) {
      quiet = false;
    } else {
      quiet = true;
      }
  }

  // Set up variables "aliases"
  ip_string = argv[1];
  port_string = argv[2];
  // Create the UDP socket.
  // AF_INET is the address family used for IPv4 addresses
  // SOCK_DGRAM indicates creation of a UDP socket
  udp_socket = socket(AF_INET, SOCK_DGRAM, 0);

  // Make sure socket was created successfully, or exit.
  if (udp_socket == -1) {
    std::cerr << "Failed to create udp socket!" << std::endl;
    std::cerr << strerror(errno) << std::endl;
    return 1;
  }

  // inet_pton converts an ip address string (e.g., 1.2.3.4) into the 4 byte
  // equivalent required for using the address in code.
  // Note that because dest_addr is a sockaddr_in (again, IPv4) the 'sin_addr'
  // member of the struct is used for the IP
  ret = inet_pton(AF_INET, ip_string, (void *)&dest_addr.sin_addr);

  // Check whether the specified IP was parsed properly. If not, exit.
  if (ret == -1) {
    std::cerr << "Failed to parse IPv4 address!" << std::endl;
    std::cerr << strerror(errno) << std::endl;
    close(udp_socket);
    return 1;
  }

  // Convert the port string into an unsigned integer.
  ret = sscanf(port_string, "%u", &port);

  // sscanf is called with one argument to convert, so the result should be 1
  // If not, exit.
  if (ret != 1) {
    std::cerr << "Failed to parse port!" << std::endl;
    std::cerr << strerror(errno) << std::endl;
    close(udp_socket);
    return 1;
  }



  // Set the address family to AF_INET (IPv4)
  dest_addr.sin_family = AF_INET;
  // Set the destination port. Use htons (host to network short)
  // to ensure that the port is in big endian format
  dest_addr.sin_port = htons(port);


//create the game message
  struct GetGameMessage game;
  //seed a random generator
  srand(time(0));
  //declare the client id, type and length
  game.client_id = htons(rand());
  game.hdr.type = htons(ClientGetGame);
  game.hdr.len = htons(sizeof(struct GetGameMessage));

  // Send the data to the destination.
  // Note 1: we are sending strlen(data_string) + 1 to include the null terminator
  // Note 2: we are casting dest_addr to a struct sockaddr because sendto uses the size
  //         and family to determine what type of address it is.
  // Note 3: the return value of sendto is the number of bytes sent
  /*ret = sendto(udp_socket, data_string, strlen(data_string) + 1, 0,
               (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in));
  */
  if(!quiet) {
    std::cout << "Will send `GetGameMessage' via UDP to " << ip_string << ":" <<port_string<< std::endl;
  }
  //send the data to server
  ret = sendto(udp_socket, &game, sizeof(struct GetGameMessage), 0, (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in));

  // Check if send worked, clean up and exit if not.
  if (ret == -1) {
    std::cerr << "Failed to send data!" << std::endl;
    std::cerr << strerror(errno) << std::endl;
    close(udp_socket);
    return 1;
  }
  if(!quiet) {
    std::cout << "Sent " << ret << " bytes out via UDP" << std::endl;
  }
  /**
   * Code to receive response from the server goes here!
   * recv or recvfrom...
   */

   //get the data back from server, put into buffer
   char *buff = new char[sizeof(GameSummaryMessage)];
   int res = recv(udp_socket, buff, sizeof(GameSummaryMessage), 0);
   if(!quiet) {
     std::cout << "REC " << res << " bytes from " << ip_string << ":" <<port_string<<std::endl;
   }
   //create a summary
   struct GameSummaryMessage *sum;

  sum = (struct GameSummaryMessage *)buff;
  sum->hdr.type = ntohs(sum->hdr.type);
  sum->hdr.len = ntohs(sum->hdr.len);
 	sum->client_id = ntohs(sum->client_id);;
 	sum->game_id = ntohs(sum->game_id);
 	sum->x_positions = ntohs(sum->x_positions);
 	sum->o_positions = ntohs(sum->o_positions);

  if(!quiet) {
  std::cout << "Received game result length " << sum->hdr.len << " client ID " << sum->client_id <<" game ID "<< sum->game_id << std::endl;
  std::cout <<"x positions: " <<sum->x_positions<< ", o positions: " << sum->o_positions << std::endl;
  }
  char board[9];

  struct GameResultMessage result;
  result.hdr.type = htons(ClientResult);
  result.hdr.len = htons(sizeof(GameResultMessage));
  result.game_id = htons(sum->game_id);
  result.result = 0;

  bool flag = false;
  //check to see if there are any x's in o's positions, and create the board array
  for(int i = 0; i<9;i++) {
    board[i] = ' ';
  }
  for(int i = 0; i<9;i++) {
    int xtemp = (sum->x_positions >> i) & 1;
    int ytemp = (sum->o_positions >> i) & 1;
    if(xtemp==1 && ytemp == 1) {
    result.result = htons(INVALID_BOARD);
    board[i] = 'B';
    flag = true;
    }
    else if(xtemp == 1) {
      board[i] = 'X';
    } else if(ytemp == 1) {
      board[i] = 'O';
    }   else {
      board[i] = ' ';
    }

  }

//output the board
  if(!quiet) {
    std::cout << "     |     |     " << std::endl
              << "  "<<board[0]<<"  |  "<<board[1]<<"  |  "<<board[2]<<"  " << std::endl
              << "_____|_____|_____" << std::endl
              << "     |     |     " << std::endl
              << "  "<<board[3]<<"  |  "<<board[4]<<"  |  "<<board[5]<<"  " << std::endl
              << "_____|_____|_____" << std::endl
              << "     |     |     " << std::endl
              << "  "<<board[6]<<"  |  "<<board[7]<<"  |  "<<board[8]<<"  " << std::endl
              << "     |     |     " << std::endl;
  }

    bool xtemp = false;
    bool ytemp = false;


  //determine the win conditions to see who won.
  if((board[0]=='X' && board[1]=='X' && board[2]=='X') ||
     (board[3]=='X' && board[4]=='X' && board[5]=='X') ||
     (board[6]=='X' && board[7]=='X' && board[8]=='X') ||
     (board[0]=='X' && board[3]=='X' && board[6]=='X') ||
     (board[1]=='X' && board[4]=='X' && board[7]=='X') ||
     (board[2]=='X' && board[5]=='X' && board[8]=='X') ||
     (board[0]=='X' && board[4]=='X' && board[8]=='X') ||
     (board[2]=='X' && board[4]=='X' && board[6]=='X')) {
       result.result = htons(X_WIN);
       xtemp = true;
     }
  if ((board[0]=='O' && board[1]=='O' && board[2]=='O') ||
        (board[3]=='O' && board[4]=='O' && board[5]=='O') ||
        (board[6]=='O' && board[7]=='O' && board[8]=='O') ||
        (board[0]=='O' && board[3]=='O' && board[6]=='O') ||
        (board[1]=='O' && board[4]=='O' && board[7]=='O') ||
        (board[2]=='O' && board[5]=='O' && board[8]=='O') ||
        (board[0]=='O' && board[4]=='O' && board[8]=='O') ||
        (board[2]=='O' && board[4]=='O' && board[6]=='O')) {
          result.result = htons(O_WIN);
          ytemp = true;
        }
  if(xtemp == false && ytemp == false) {
          result.result = htons(CATS_GAME);
      }
  if (xtemp && ytemp) {
        flag = true;
      }

    //  result.result = htons(result.result);
    if(flag) {
      result.result = htons(INVALID_BOARD);
    }
    //send the result
    ret = sendto(udp_socket, &result, sizeof(struct GameResultMessage), 0,
                 (struct sockaddr *)&dest_addr, sizeof(struct sockaddr_in));
    if(!quiet) {
      std::cout << "Sent " << ret << " bytes out via UDP" << std::endl;
  }
    // Check if send worked, clean up and exit if not.
    if (ret == -1) {
      std::cerr << "Failed to send data!" << std::endl;
      std::cerr << strerror(errno) << std::endl;
      close(udp_socket);
      return 1;
    }

    //get the message again
    buff = new char[sizeof(TTTMessage)];
    res = recv(udp_socket, buff, sizeof(TTTMessage), 0);

    //create a TTTmessage
    struct TTTMessage *message;
    message = (struct TTTMessage *)buff;
    message->type = ntohs(message->type);
    message->len = ntohs(message->len);

    std::string strtype = " ";
    //output the result from the server.
    if(message->type == 115) {
      strtype = "CORRECT";
    } else if(message->type == 116) {
      strtype = "INCORRECT";
    } else {strtype = "Unsure";}

    std::string expect = " ";
    if(ntohs(result.result) == 11) {
      expect = "X_WIN";
    } else if(ntohs(result.result) == 12) {
      expect = "O_WIN";
    } else if(ntohs(result.result) == 13) {
      expect = "CATS_GAME";
    } else if(ntohs(result.result) == 14) {
      expect = "INVALID_BOARD";
    } else {
      expect = "UNSURE";
    }
    if(!quiet) {
      std::cout << "REC " << res << " bytes from " << ip_string << ":" <<port_string<<std::endl;
    }
    std::cout << "Got " << strtype << " from the server, Expected: "<< expect<< std::endl;
  close(udp_socket);
  return 0;
}
